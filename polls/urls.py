from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from . import views
from views import TestProject1

urlpatterns = [
       url(r'^shortener/$',
           csrf_exempt(TestProject1.as_view())),
       url(r'^shortener/list/$',views.getlist,name='Shortener'),       


   ]
