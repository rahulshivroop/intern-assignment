# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from .models import url_shortener

def getlist(request):
        return HttpResponse(url_shortener.objects.all())

# Create your views here.
class TestProject1(View):

        def post(self, request):
                params = request.POST
                input1 = params.get('input1')
                input2=  params.get('input2')
                object1= url_shortener.objects.create(shortener=input1,url=input2)
                object1.save()
                try:

                    return HttpResponse(url_shortener.objects.all())


                except (TypeError, ValueError):
                    return HttpResponse('Invalid values')

        def delete(self,request):
                params= request.GET
                input=params.get('input')
                snippet = url_shortener.objects.get(shortener=input)
                snippet.delete()
                snippet.save()
                try:

                    return HttpResponse(url_shortener.objects.all())
                except ():
                    return HttpResponse('Invalid values')



        def get(self,request):
                params= request.GET
                input=params.get('input')
                snippet = url_shortener.objects.get(shortener=input)
                try:

                    return HttpResponse(snippet)


                except (TypeError, ValueError):
                    return HttpResponse('Invalid values')
