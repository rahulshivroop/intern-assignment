# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class url_shortener(models.Model):
    shortener = models.CharField(primary_key="TRUE",max_length=30)
    url = models.CharField(max_length=30)
    def __str__(self):
        return ' '+self.shortener+" "+self.url
