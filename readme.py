Introduction-
This is a URL_shortener service where a URL and its shortened version is stored in a database.

Functions-
1.POST /shortener/ -> Create Shortener
If a post request is sent to the url (http://0.0.0.0:8081/dj/shortener/) with the params as input1 and input2 in the body a new entry with the URL and Shortener is created

2.GET /shortener/list -> Return list of Url shortener
IF a get request with no params is sent to http://0.0.0.0:8081/dj/shortener/list, the entire list of url and shortener is returned.

3.DELETE /shortener/<shortener> -> Delete entry from DB
IF a delete request with param as input is sent to http://0.0.0.0:8081/dj/shortener/, that particular shortener and url entry is deleted from the database

4.GET /<shortener> -> Redirects to URL
IF a GET request with shortener as input is sent to http://0.0.0.0:8081/dj/shortener/, that particular shortener and url entry is retrieved from the database
